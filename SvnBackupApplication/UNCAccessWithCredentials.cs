using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using DWORD = System.UInt32;
using LPWSTR = System.String;
using NET_API_STATUS = System.UInt32;

namespace ConnectUNCWithCredentials
{
    /// <summary>
    /// Remote file share connection class
    /// </summary>
    public class UNCAccessWithCredentials : IDisposable
    {
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct USE_INFO_2
        {
            internal LPWSTR ui2_local;
            internal LPWSTR ui2_remote;
            internal LPWSTR ui2_password;
            internal DWORD ui2_status;
            internal DWORD ui2_asg_type;
            internal DWORD ui2_refcount;
            internal DWORD ui2_usecount;
            internal LPWSTR ui2_username;
            internal LPWSTR ui2_domainname;
        }

        [DllImport("NetApi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        internal static extern NET_API_STATUS NetUseAdd(
            LPWSTR UncServerName,
            DWORD Level,
            ref USE_INFO_2 Buf,
            out DWORD ParmError);

        [DllImport("NetApi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        internal static extern NET_API_STATUS NetUseDel(
            LPWSTR UncServerName,
            LPWSTR UseName,
            DWORD ForceCond);

        [DllImport("NetApi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        internal static extern NET_API_STATUS NetUseEnum(
            LPWSTR UncServerName,
            DWORD Level,
            ref IntPtr Buf,
            DWORD PreferedMaximumSize,
            out int EntriesRead,
            out int TotalEntries,
            IntPtr resumeHandle);

        [DllImport("Netapi32.dll", SetLastError = true)]
        internal static extern int NetApiBufferFree(IntPtr Buffer);

        [DllImport("mpr.dll")]
        internal static extern int WNetCancelConnection2(string lpName, Int32 dwFlags, bool bForce);

        private bool disposed = false;

        private string sUNCPath;
        private string sUser;
        private string sPassword;
        private string sDomain;
        private int iLastError;

        /// <summary>
        /// A disposeable class that allows access to a UNC resource with credentials.
        /// </summary>
        public UNCAccessWithCredentials()
        {
        }

        /// <summary>
        /// The last system error code returned from NetUseAdd or NetUseDel.  Success = 0
        /// </summary>
        public int LastError
        {
            get { return iLastError; }
        }

        public void Dispose()
        {
            if (!this.disposed)
            {
                NetUseDelete();
            }
            disposed = true;
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Connects to a UNC path using the credentials supplied.
        /// </summary>
        /// <param name="UNCPath">Fully qualified domain name UNC path</param>
        /// <param name="User">A user with sufficient rights to access the path.</param>
        /// <param name="Domain">Domain of User.</param>
        /// <param name="Password">Password of User</param>
        /// <returns>True if mapping succeeds.  Use LastError to get the system error code.</returns>
        public bool NetUseWithCredentials(string UNCPath, string User, string Domain, string Password)
        {
            sUNCPath = UNCPath;
            sUser = User;
            sPassword = Password;
            sDomain = Domain;
            return NetUseWithCredentials();
        }

        private bool NetUseWithCredentials()
        {
            uint returncode;
            try
            {
                USE_INFO_2 useinfo = new USE_INFO_2();

                useinfo.ui2_remote = sUNCPath;
                useinfo.ui2_username = sUser;
                useinfo.ui2_domainname = sDomain;
                useinfo.ui2_password = sPassword;
                useinfo.ui2_asg_type = 0;
                useinfo.ui2_usecount = 1;
                uint paramErrorIndex;
                returncode = NetUseAdd(null, 2, ref useinfo, out paramErrorIndex);
                iLastError = (int)returncode;
                return returncode == 0;
            }
            catch
            {
                iLastError = Marshal.GetLastWin32Error();
                return false;
            }
        }

        /// <summary>
        /// Ends the connection to the remote resource 
        /// </summary>
        /// <returns>True if it succeeds.  Use LastError to get the system error code</returns>
        public bool NetUseDelete()
        {
            uint returncode;
            try
            {
                returncode = NetUseDel(null, sUNCPath, 2);
                iLastError = (int)returncode;
                return (returncode == 0);
            }
            catch
            {
                iLastError = Marshal.GetLastWin32Error();
                return false;
            }
        }

        /// <summary>
        /// Gets all currently connected network shares. Code from pinvoke.net
        /// </summary>
        /// <returns>Enumeration of network shares</returns>
        public static IEnumerable<USE_INFO_2> NetUseAllCurrentConnections()
        {
            IntPtr lBuffer = IntPtr.Zero;
            int lRead;
            int lTotal;
            IntPtr lHandle = IntPtr.Zero;

            NetUseEnum(null, 2, ref lBuffer, 0xffffffff, out lRead, out lTotal, lHandle);

            // now step through all network shares and check if we have already a connection to the server
            int li = 0;
            USE_INFO_2 lInfo;
            while (li < lRead)
            {
                IntPtr ptr = IntPtr.Add(lBuffer, Marshal.SizeOf(typeof(USE_INFO_2)) * li);
                lInfo = (USE_INFO_2)Marshal.PtrToStructure(ptr, typeof(USE_INFO_2));

                yield return lInfo;

                ++li;
            }

            NetApiBufferFree(lBuffer);
        }

        /// <summary>
        /// Deletes all connected network shares, which contain specific string in their name
        /// </summary>
        /// <param name="shareName"></param>
        public static void NetUseDeleteConnectionsToShare(string shareName)
        {
            foreach (var unc in NetUseAllCurrentConnections())
            {
                if (!unc.ui2_remote.Contains(shareName)) continue;

                var returnCode = string.IsNullOrEmpty(unc.ui2_local) ? 
                    Convert.ToInt32(NetUseDel(null, unc.ui2_remote, 2)) :
                    WNetCancelConnection2(unc.ui2_local, 0x1, true);
                
                if (returnCode != 0)
                {
                    throw new Win32Exception(returnCode);
                }
            }
        }

        ~UNCAccessWithCredentials()
        {
            Dispose();
        }
    }
}