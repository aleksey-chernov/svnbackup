﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CommandLine;
using CommandLine.Text;
using ConnectUNCWithCredentials;

namespace SvnBackupApplication
{
    internal class Program
    {
        private class Options
        {
            [Option('s', "src", Required = true, HelpText = "Source path, where SVN repositories are located (network or local).")]
            public string SourcePath { get; set; }

            [Option('d', "dest", Required = true, HelpText = "Destination path (network or local).")]
            public string DestinationPath { get; set; }

            [Option('f', "full", HelpText = "Select for full dump.")]
            public bool IsFull { get; set; }

            [OptionList("srccred", Separator = ':', HelpText = "Specify credentials for source path connection, if source is a network share." +
                                                               " In form domain:user:password")]
            public IList<string> SourceCredentials { get; set; }

            [OptionList("destcred", Separator = ':', HelpText = "Specify credentials for destination path connection, if destination is a network share." +
                                                               " In form domain:user:password")]
            public IList<string> DestintaionCredentials { get; set; }

            [HelpOption]
            public string GetUsage()
            {
                var help = new HelpText
                {
                    AdditionalNewLineAfterOption = true,
                    AddDashesToOption = true
                };
                help.AddOptions(this);

                return help;
            }
        }

        private static void Main(string[] args)
        {
            UNCAccessWithCredentials sourceConnection = null;
            UNCAccessWithCredentials destConnection = null;

            var options = new Options();

            var parser = new Parser(conf =>
            {
                conf.CaseSensitive = false;
                conf.IgnoreUnknownArguments = true;
                conf.HelpWriter = Console.Error;
            });

            if (parser.ParseArguments(args, options))
            {
                try
                {
                    if (options.SourceCredentials != null && options.SourceCredentials.Count > 2)
                    {
                        if (new Uri(options.SourcePath).IsUnc)
                        {
                            UNCAccessWithCredentials.NetUseDeleteConnectionsToShare(options.SourcePath);

                            sourceConnection = new UNCAccessWithCredentials();
                            sourceConnection.NetUseWithCredentials(options.SourcePath, options.SourceCredentials[1],
                                options.SourceCredentials[0], options.SourceCredentials[2]);
                        }
                    }

                    if (options.DestintaionCredentials != null && options.DestintaionCredentials.Count > 2)
                    {
                        if (new Uri(options.DestinationPath).IsUnc)
                        {
                            UNCAccessWithCredentials.NetUseDeleteConnectionsToShare(options.DestinationPath);

                            destConnection = new UNCAccessWithCredentials();
                            destConnection.NetUseWithCredentials(options.DestinationPath, options.DestintaionCredentials[1],
                                options.DestintaionCredentials[0], options.DestintaionCredentials[2]);
                        }
                    }

                    Backup(options.SourcePath, options.DestinationPath, options.IsFull);

                    if (sourceConnection != null)
                        sourceConnection.NetUseDelete();

                    if (destConnection != null)
                        destConnection.NetUseDelete();
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.Message);
                    Environment.ExitCode = 1;
                }
            }
            else
            {
                Environment.ExitCode = 1;
            }
        }

        private static void Backup(string sourcePath, string destPath, bool fullDump = false)
        {
            if (string.IsNullOrEmpty(sourcePath) || string.IsNullOrEmpty(destPath))
                return;

            var dateString = DateTime.Now.ToString("yyyy-MM-dd");

            var backupPath = Path.Combine(destPath, dateString, "backup");
            if (Directory.Exists(backupPath))
            {
                DeleteDirectory(backupPath);
            }
            Directory.CreateDirectory(backupPath);
            
            var dumpPath = Path.Combine(destPath, dateString, "dump");
            if (Directory.Exists(dumpPath))
            {
                DeleteDirectory(dumpPath);
            }
            Directory.CreateDirectory(dumpPath);
            
            foreach (var file in Directory.GetFiles(sourcePath).Select(file => new FileInfo(file)))
            {
                file.CopyTo(Path.Combine(backupPath, file.Name), true);
            }

            var cmdStartInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = "cmd.exe"
            };
            foreach (var dir in new DirectoryInfo(sourcePath).GetDirectories("*.*", SearchOption.TopDirectoryOnly))
            {
                cmdStartInfo.Arguments = string.Format(@"/C svnadmin hotcopy {0} {1}", dir.FullName, 
                    Path.Combine(backupPath, dir.Name));

                var process = new Process
                {
                    StartInfo = cmdStartInfo
                };
                process.Start();
                process.WaitForExit();

                if (!fullDump) continue;

                var tempDump = Path.Combine(sourcePath, dir.Name + "_dump");
                cmdStartInfo.Arguments = string.Format(@"/C svnadmin dump {0} > {1}", dir.FullName, tempDump);
                
                process = new Process
                {
                    StartInfo = cmdStartInfo
                };
                process.Start();
                process.WaitForExit();

                if(!File.Exists(tempDump)) continue;

                var tempDumpFile = new FileInfo(tempDump);
                tempDumpFile.CopyTo(Path.Combine(dumpPath, dir.Name), true);

                File.SetAttributes(tempDump, FileAttributes.Normal);
                File.Delete(tempDump);
            }
        }

        private static void DeleteDirectory(string targetDir)
        {
            File.SetAttributes(targetDir, FileAttributes.Normal);

            var files = Directory.GetFiles(targetDir);
            var dirs = Directory.GetDirectories(targetDir);

            foreach (var file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            foreach (var dir in dirs)
            {
                DeleteDirectory(dir);
            }

            Directory.Delete(targetDir, false);
        }
    }
}